# HowTo HelloWorld with Docker & Node.js

0. Prerequisite
- WSL2 is ready on your Windows machine
- you have installed an version of linux (e.g. Ubunto 20.xx)
- in linux internet connection works fine (run `ping google.de` to check)
- docker is installed (run: `docker --version` to see if there is an output)
- docker-compose is installed (run `docker-compose --version` to see if there is an output)
- node.js is installed (run: `node --version` to see if there is an output)
- if one of them doesn't work stop here. In the doc directory you find the installation guides.

## Install
1. Create a directory with a name you like in a directory you prefer (`makedir [dir name]`)

2. Put the content of this project in the directory you just created an `cd` into it (e.g. by `git clone [repo] .`)

3. Run: `npm install` (it takes some time, ~ 3-4 minutes)
- now you have a file package-lock.json and a directory node_modules

4. To build a docker image run: `docker build --tag [name it as you like] .`
- e.g. `docker build --tag docker-helloworld .`
- don't forget the `.` at the end
- if you got "permission denied ..." run all docker commands with `sudo ...`
- if you got "Successfully tagged docker-helloworld:latest" all is fine

5. You can see all of your local docker images if you run: 
- `sudo docker images`

## Production Mode
6. For production now start a container. Run 
- `sudo docker run -d -p 3000:3000 docker-helloworld` (the name depends on the tag you gave the container in step 4)
- the -d option stands for --detach (run container in the background)
- the -p option stands for --publish
- 3000:3000 maps the port we specified in index.js to a port we can access
- if you got a magic string in return (e.g. "7068522fc4...ea53a5da44e") all seems to be fine

7. Go to the browser and look up for http://127.0.0.1:3000/
- if you see "Hello World!" - let's dance :-)

8. You can see all of your local running docker containers if you run
- `sudo docker ps`
- in the list you can see that docker automaticly gives the running container a fancy name (e.g. "quizzical_ishizaka")
- to stop a container run: `sudo docker stop quizzical_ishizaka`
- to restart a container run: `sudo docker restart quizzical_ishizaka`
- to remove a container run: `sudo docker rm quizzical_ishizaka`
- "quizzical_ishizaka" is just an example - on your machine it's completly different

9. For production mode all magic is in the Dockerfile - Have a look to it's content. I tried to make a lot of comments what's going on there

## Developing Mode
10. The way we start a docker container for developing mode is different from the one discribed under step 6. 

11. First be shure you `cd` in the directory you created for this project in step 1. Then type:
- `code .`
- don't forget the . at the end
- on the first run wsl/linux installes some necessary stuff
- Visual Code Studio is starting and opens the directory 
- if it dos not, may be we have to install Visual Studio Code Server manually, up to now I have no HowTo

12. To start a docker container for developing we use docker-compose and give it some config via docker-compose.dev.yml. So run:
- `docker-compose -f docker-compose.dev.yml up --build`
- if you see some messages saying "Successfully..." and colored output saying in the end "Example app listening at http://127.0.0.1:3000" all seems to be fine. You can see the output in your browser again like in production mode

13. Via VS Code you can now update the index.js (e.g. write "Hello Cologne") and see the changes in the browser without restarting the container again

14. Use `Ctrl + C` to stop the container

14. What's the magic behind?
- To make it work we had to add "nodemon" as one of the "devDependencies" in the package.json. 
- In the section "scripts" of package.json we added "debug": "nodemon --inspect..." with a port we create in docker-compose.dev.yml at which it is listening for updates (have a look in the package.json file)
- All other things are done in the docker-compose.dev.yml: mapping the necessary ports 3000 and 9229; mounting (not copying like in production mode) the directory in the container; running npm in debug mode (have a look in the docker-compose.dev.yml file)

## Sources
HowTo is based on https://docs.docker.com/language/nodejs/ 