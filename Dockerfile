# syntax=docker/dockerfile:1

# tell Docker to include in our image all the functionality from the node:16.0.0 image / alpine version
FROM mhart/alpine-node:16.0.0

# One of the simplest things you can do to improve performance is to set NODE_ENV to production
ENV NODE_ENV=production

# This instructs Docker to use this path as the default location for all subsequent commands
WORKDIR /app

# Before we can run npm install, we need to get our package.json and package-lock.json files into our images
# COPY takes two parameters: 1 tells Docker what file(s) you would like to copy into the image. 2 tells where you want that file(s) to be copied to
# files will be copied into our working dir

COPY ["package.json", "package-lock.json*", "./"]

# now we can RUN command to execute npm install like running npm install locally, 
# but now Node modules will be installed into the node_modules directory inside our image
RUN npm install --production

# next we need to add our source code into the image
COPY . .

# Now, we have to tell Docker what command we want to run. We do this with the CMD command
CMD [ "node", "index.js" ]

# additionaly it s usefull to add a .dockerignore file and add the folder node_modules in it 
# so this wont be copied in step COPY . .
