1. In Linux create a file: /etc/wsl.conf
sudo vi /etc/wsl.conf

2. To ensure your DNS changes do not get blown away – put the following into it and save:

[network]
generateResolvConf = false


3. In Windows open a cmd window and run 

wsl --shutdown

4. Restart WSL2/Ubuntu

5. In the directory /etc there is a sym link:

resolv.conf -> ../run/resolvconf/resolv.conf

delete it by typing

sudo rm resolv.conf

6. Then create this as a file:

sudo vi /etc/resolv.conf

and put some nameserver (e.g. google) into it:

nameserver 8.8.8.8

If you are using a vpn you have to add the DNS-Server IPs of the vpn.

7. Save it and repeat step 3 + 4
