1. Update the apt package index
sudo apt-get update

2. Install packages to allow apt to use a repository over HTTPS
sudo apt-get install apt-transport-https ca-certificates curl gnupg lsb-release

3. Add Docker’s official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

4. Use the following command to set up the stable repository
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

5. Update the apt package index again
sudo apt-get update

6. Install Docker
sudo apt-get install docker-ce docker-ce-cli containerd.io

7. Check installation

docker --version

and/or

sudo docker run hello-world

8. HowTo based on
https://docs.docker.com/engine/install/ubuntu/