1. Run this command to download the current stable release of Docker Compose:
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

2. Apply executable permissions to the binary:
sudo chmod +x /usr/local/bin/docker-compose

3. Create a symbolic link to be able to run docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

4. Test installation by typing
docker-compose --version

5. Based on
https://docs.docker.com/compose/install/
Choose tab [Linux]